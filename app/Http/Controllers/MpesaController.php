<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SmoDav\Mpesa\C2B\Registrar;

class MpesaController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function c2b_simulation()
    {
        return view('c2b_simulate');
    }

    public function c2b_form(Request $request)
    {
        $tel = $request->input('tel');
        $amount = $request->input('amount');
        $ref = $request->input('ref');//substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 9);

        $response = \Simulate::request($amount)
        ->from('2547'.$tel)
        ->usingReference('.$ref.')
        ->setCommand(CUSTOMER_PAYBILL_ONLINE)
        ->push();
    }

    public function c2b_stk_push()
    {
        return view('c2b_stk_push');
    }

    public function c2b_stk_form(Request $request)
    {
        $tel = $request->input('tel');
        $amount = $request->input('amount');
        $ref = $request->input('ref');

        $response = \STK::push($amount,'2547'.$tel, '.$ref.', 'Test Payment');
    }

    public function validate_no()
    {
        return 'validate_no';
    }

    public function ConfirmationURL(Request $request)
    {
        dd($request);
    }

    public function registerUrl()
    {
        $response = \Registrar::register(601452)
        ->onConfirmation('http://139.59.68.144/mpesa/ConfirmationURL')
        ->onValidation('http://139.59.68.144/mpesa/ValidationURL')
        ->submit();

        //return $response;
    }
}
