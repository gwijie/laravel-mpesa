<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','MpesaController@index')->name('mpesa.index');

Route::get('c2b_simulation_transaction','MpesaController@c2b_simulation')->name('mpesa.c2b_simulation');

Route::post('c2b_form','MpesaController@c2b_form')->name('mpesa.c2b_form');
Route::post('c2b_stk_form','MpesaController@c2b_stk_form')->name('mpesa.c2b_stk_form');

Route::get('c2b_stk_push','MpesaController@c2b_stk_push')->name('mpesa.c2b_stk_push');
Route::get('validate_no','MpesaController@validate_no')->name('mpesa.validate_no');

Route::get('mpesa/ValidationURL','MpesaController@ValidationURL')->name('mpesa.ValidationURL');
Route::get('mpesa/ConfirmationURL','MpesaController@ConfirmationURL')->name('mpesa.ConfirmationURL');

Route::get('mpesa/registerUrl','MpesaController@registerUrl')->name('mpesa.registerUrl');
